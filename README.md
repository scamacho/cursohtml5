# README #

Pequeña aplicación para llevar una lista de tareas. 

### Funciones actuales ###

* Agregar una tarea usando el botón ![Alt text](img/PNG/Black/12.File.png) "Agregar tarea" del menú (el campo "Grupo" aún no es funcional)
* Mostrar notificaciones cuando se cumple la fecha y hora de vencimiento de la tarea
* Marcar una tarea como completa y borrarla de la lista (usando el botón a la derecha de la tarea)