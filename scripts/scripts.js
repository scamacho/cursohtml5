(function (){
// create an instance of a db object to store the IDB data in
var db;

// create a blank instance of the object that is used to transfer data into the IDB. This is mainly for reference
var newItem = [{
    taskName: "",
    date: 0,
    time: 0,
    notified: "no"
}];
// all the variables needed for the app    
var taskName = document.getElementById("task-name");
var taskDate = document.getElementById("task-date");
var taskTime = document.getElementById("task-time");
var countoverdue = 0;

var taskList = document.querySelector('#todayList tbody');

var taskWindow = document.getElementById("new-task");

var taskForm = document.getElementById('task');

var addButton = document.getElementById('addTask');

var toggleMenuButton = document.getElementById('toggleMenu');

var toggleBarButton = document.getElementsByClassName("toggleBar")[0];

var menu = document.getElementById("nav-container");

var sideBar = document.getElementById("sidebar");

window.onload = function() {
    addButton.addEventListener('click', showForm, false);

    toggleBarButton.addEventListener('click', toggleBar, false);

    toggleMenuButton.addEventListener('click', toggleMenu, false);

    document.getElementsByClassName("closeForm")[0].addEventListener('click',closeForm,false);

    // give the form submit button an event listener so that when the form is submitted the addData() function is run
    taskForm.addEventListener('submit', addTask, false);
    
    // In the following line, you should include the prefixes of implementations you want to test.
    window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    // DON'T use "var indexedDB = ..." if you're not in a function.
    // Moreover, you may need references to some window.IDB* objects:
    window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
    window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
    

    // Let us open our database
    var DBOpenRequest = window.indexedDB.open("toDoList", 5);

    // Gecko-only IndexedDB temp storage option:
    // var request = window.indexedDB.open("toDoList", {version: 4, storage: "temporary"});

    // these two event handlers act on the database being opened successfully, or not
    DBOpenRequest.onerror = function(event) {
        console.log("Error loading database");
    };

    DBOpenRequest.onsuccess = function(event) {
        console.log("Database intialised");

        // store the result of opening the database in the db variable. This is used a lot below
        db = DBOpenRequest.result;

        // Run the displayData() function to populate the task list with all the to-do list data already in the IDB
        displayData();
    };

    // This event handles the event whereby a new version of the database needs to be created
  // Either one has not been created before, or a new version number has been submitted via the
  // window.indexedDB.open line above
  //it is only implemented in recent browsers
  DBOpenRequest.onupgradeneeded = function(event) { 
    var db = event.target.result;
    
    db.onerror = function(event) {
      console.log('Error loading database.');
    };

    // Create an objectStore for this database
    
    var objectStore = db.createObjectStore("toDoList", { keyPath: "taskName" });
    
    // define what data items the objectStore will contain
    
    objectStore.createIndex("time", "hours", { unique: false });
    objectStore.createIndex("date", "date", { unique: false });

    objectStore.createIndex("notified", "notified", { unique: false });
    
    console.log('Object store created.');
  };

    function toggleMenu(e){
        if(menu.style.display=="block"){
            menu.style.display="none";
        }
        else{
            menu.style.display="block";
        }
    };

    function showForm(e) {
        e.preventDefault();
        taskWindow.style.display="block";
        taskWindow.style.zIndex="1000";
        if(menu.style.display=="block"){
            menu.style.display="none";
        }
    };

    function closeForm (e) {
        var visibleForm = this.parentElement;
        visibleForm.style.display="none";
        visibleForm.style.zIndex="0";
    };

    function toggleBar (e) {
      var listItems = sideBar.querySelectorAll("ul");
      for(var i=0; i<listItems.length;i++){
          if(listItems[i].classList.contains("visible")){
                listItems[i].classList.remove("visible");
                listItems[i].classList.add("hidden");
          }else{
            listItems[i].classList.remove("hidden");
            listItems[i].classList.add("visible");
          }
      }
    	if(sideBar.style.right=="80%"){ //ocultar
    	   sideBar.children[0].style.left="0";
    	   sideBar.style.right="100%";
    	   sideBar.style.width="auto";
    	}
    	else{//mostrar
        if(menu.style.display=="block"){
            menu.style.display="none";
        }
    	  sideBar.children[0].style.left ="95%";
    	  sideBar.style.right="80%";
    	  sideBar.style.width="35%";
    	}
    };

    function displayData () {
    	// first clear the content of the task list so that you don't get a huge long list of duplicate stuff each time
    //the display is updated.
    taskList.innerHTML = "";
    countOverdue =0;
  
    // Open our object store and then get a cursor list of all the different data items in the IDB to iterate through
    var objectStore = db.transaction('toDoList').objectStore('toDoList');
    objectStore.openCursor().onsuccess = function(event) {
      var cursor = event.target.result;
        // if there is still another cursor to go, keep runing this code
        if(cursor) {
          // create a list item to put each data item inside when displaying it
          var listItem = document.createElement('tr');
          
          var cell = document.createElement('td');
          cell.innerHTML='<span class="status"></span>';
          listItem.appendChild(cell);
          cell=document.createElement('td');
          cell.innerHTML=cursor.value.taskName;
          listItem.appendChild(cell);
          cell= document.createElement('td');
          cell.innerHTML=cursor.value.date;
          listItem.appendChild(cell);
          cell= document.createElement('td');
          cell.innerHTML=cursor.value.time;
          listItem.appendChild(cell);
          
          if(cursor.value.notified == "yes") {
            listItem.querySelector(".status").classList.add("overdue");
            ++countOverdue;
      
          }

          // put the item item inside the task list
          taskList.appendChild(listItem);  
          
          // create a complete button inside each list item, giving it an event handler so that it runs the completed()
          // function when clicked
          var completeButton = document.createElement('button');
          listItem.appendChild(completeButton);
          completeButton.innerHTML = '<img src="img/PNG/Black/42.Badge.png" alt="Complete"/>';
          // here we are setting a data attribute on our delete button to say what task we want deleted if it is clicked! 
          completeButton.setAttribute('data-task', cursor.value.taskName);
          completeButton.onclick = function(e) {
            completed(e);
          }
          
          // continue on to the next item in the cursor
          cursor.continue();
        
        // if there are no more cursor items to iterate through, say so, and exit the function 
        } else {
          console.log('Entries all displayed.');
          document.querySelector(".count.overdue").innerHTML=countOverdue;
        }
      }
    };

    function addTask(e) {
        // prevent default - we don't want the form to submit in the conventional way
        e.preventDefault();

        // Stop the form submitting if any values are left empty. This is just for browsers that don't support the HTML5 form
        // required attributes
        if (taskName.value == '' || taskDate.value == null || taskTime.value == null ) {
            return;
        } else {

            // grab the values entered into the form fields and store them in an object ready for being inserted into the IDB
            var newItem = [{
                taskName: taskName.value,
                date: taskDate.value,
                time: taskTime.value,
                notified: "no"
            }];

            // open a read/write db transaction, ready for adding the data
            var transaction = db.transaction("toDoList", "readwrite");

            // report on the success of opening the transaction
            transaction.oncomplete = function(e) {
                console.log('Transaction completed: database modification finished.');

                // update the display of data to show the newly added item, by running displayData() again.
                displayData();
            };

            transaction.onerror = function(e) {
                console.log('Transaction not opened due to error: ' + transaction.error);
            };

            // call an object store that's already been added to the database
            var objectStore = transaction.objectStore("toDoList");
            console.log(objectStore.indexNames);
            console.log(objectStore.keyPath);
            console.log(objectStore.name);
            console.log(objectStore.transaction);
            console.log(objectStore.autoIncrement);

            // add our newItem object to the object store
            var objectStoreRequest = objectStore.add(newItem[0]);
            objectStoreRequest.onsuccess = function(event) {

                // report the success of our new item going into the database
                console.log('New item added to database.');

                // clear the form, ready for adding the next entry
                taskName.value = '';                
                taskDate.value = null;
                taskTime.value = null;

            };

        };

    };

  function completed(e){
    // retrieve the name of the task we want to delete 
    var dataTask = e.target.parentNode.getAttribute('data-task');

    // open a database transaction and delete the task, finding it by the name we retrieved above
    var transaction = db.transaction(["toDoList"], "readwrite");
    var request = transaction.objectStore("toDoList").delete(dataTask);

    // report that the data item has been deleted
    transaction.oncomplete = function() {
      // delete the button, of the list item, and change the color of the status
      e.target.parentNode.parentNode.querySelector('.status').classList.add("complete");
      e.target.parentNode.parentNode.removeChild(e.target.parentNode);
      document.querySelector(".count.overdue").innerHTML=--countOverdue;
      
      console.log('Task \"' + dataTask + '\" marked as complete and deleted.');
    };
  }
    // function for creating the notification
  function createNotification(title) {
    // Let's check if the browser supports notifications
    if (!"Notification" in window) {
      console.log("This browser does not support notifications.");
    }

    // Let's check if the user is okay to get some notification
    else if (Notification.permission === "granted") {
      // If it's okay let's create a notification
      
      var img = 'img/PNG/Black/43.Bell.png';
      var text = 'HEY! La tarea "' + title + '" ha vencido.';
      var notification = new Notification('To do list', { body: text, icon: img });
      
      window.navigator.vibrate(500);
    }

    // Otherwise, we need to ask the user for permission
    // Note, Chrome does not implement the permission static property
    // So we have to check for NOT 'denied' instead of 'default'
    else if (Notification.permission !== 'denied') {
      Notification.requestPermission(function (permission) {

        // Whatever the user answers, we make sure Chrome stores the information
        if(!('permission' in Notification)) {
          Notification.permission = permission;
        }

        // If the user is okay, let's create a notification
        if (permission === "granted") {
          var img = 'img/PNG/43.Bell.png';
          var text = 'HEY! La tarea "' + title + '" ha vencido.';
          var notification = new Notification('To do list', { body: text, icon: img });
          
          window.navigator.vibrate(500);
        }
      });
    }

    // At last, if the user already denied any notification, and you 
    // want to be respectful there is no need to bother him any more.

    // now we need to update the value of notified to "yes" in this particular data object, so the
    // notification won't be set off on it again

    // first open up a transaction as usual
    var objectStore = db.transaction(['toDoList'], "readwrite").objectStore('toDoList');

    // get the to-do list object that has this title as it's title
    var objectStoreTitleRequest = objectStore.get(title);

    objectStoreTitleRequest.onsuccess = function() {
      // grab the data object returned as the result
      var data = objectStoreTitleRequest.result;
      
      // update the notified value in the object to "yes"
      data.notified = "yes";
      
      // create another request that inserts the item back into the database
      var updateTitleRequest = objectStore.put(data);
      
      // when this new request succeeds, run the displayData() function again to update the display
      updateTitleRequest.onsuccess = function() {
        document.querySelector(".count.overdue").innerHTML=++countOverdue;
      
        displayData();
      }
    }
  };

  // this function checks whether the deadline for each task is up or not, and responds appropriately
  function checkDeadlines() {
    
    // grab the time and date right now 
    var now = new Date();
    
    // from the now variable, store the current minutes, hours, day of the month (getDate is needed for this, as getDay 
    // returns the day of the week, 1-7), month, year (getFullYear needed; getYear is deprecated, and returns a weird value
    // that is not much use to anyone!) and seconds
    var minuteCheck = now.getMinutes();
    var hourCheck = now.getHours();
    var dayCheck = now.getDate();
    var monthCheck = now.getMonth();
    var yearCheck = now.getFullYear();
     
    // again, open a transaction then a cursor to iterate through all the data items in the IDB   
    var objectStore = db.transaction(['toDoList'], "readwrite").objectStore('toDoList');
    objectStore.openCursor().onsuccess = function(event) {
      var cursor = event.target.result;
        if(cursor) {
          var dateArray = cursor.value.date.split("-");
          // convert the month names we have installed in the IDB into a month number that JavaScript will understand. 
          // The JavaScript date object creates month values as a number between 0 and 11.
          var monthNumber = +(dateArray[1]) -1;
          
          var timeArray = cursor.value.time.split(":");
          if(+(timeArray[0]) == hourCheck && +(timeArray[1]) == minuteCheck && +(dateArray[2]) == dayCheck && monthNumber == monthCheck && dateArray[0] == yearCheck && cursor.value.notified == "no") {
            
            // If the numbers all do match, run the createNotification() function to create a system notification
            createNotification(cursor.value.taskName);
          }
          
          // move on and perform the same deadline check on the next cursor item
          cursor.continue();
        }
        
    }
    
  };

  // using a setInterval to run the checkDeadlines() function every second
  setInterval(checkDeadlines, 1000);
}
})();